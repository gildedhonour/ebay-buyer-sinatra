require 'rubygems'
require 'bundler/setup'
require 'sinatra/base'
#require 'webrick'
#require 'webrick/https'
#require 'openssl'
require 'sinatra'
require 'sinatra/flash'

ENV['RACK_ENV'] ||= "development"
require File.dirname(__FILE__) + '/app'
if ENV['RACK_ENV'] == "production"
  require 'exceptional'
  use Rack::Exceptional, ENV['EXCEPTIONAL_KEY']
end

map "/" do
  run App
end





#SSL
# CERT_PATH = '/opt/sinatra-ebay-cert/'
# name = "/C=US/ST=SomeState/L=SomeCity/O=Organization/OU=Unit/CN=localhost"
# ca   = OpenSSL::X509::Name.parse(name)
# key = OpenSSL::PKey::RSA.new(1024)
# crt = OpenSSL::X509::Certificate.new
# crt.version = 2
# crt.serial  = 1
# crt.subject = ca
# crt.issuer = ca
# crt.public_key = key.public_key
# crt.not_before = Time.now
# crt.not_after  = Time.now + 1 * 365 * 24 * 60 * 60 # 1 year
# webrick_options = {
#   :Port               => 8443,
#   :Logger             => WEBrick::Log::new($stderr, WEBrick::Log::DEBUG),
#   :DocumentRoot       => "/ruby/htdocs",
#   :SSLEnable          => true,
#   :SSLVerifyClient    => OpenSSL::SSL::VERIFY_NONE,
#   :SSLCertificate     => crt, # OpenSSL::X509::Certificate.new(File.open(File.join(CERT_PATH, "ssl-cert-snakeoil.pem")).read),
#   :SSLPrivateKey      => key, #OpenSSL::PKey::RSA.new(File.open(File.join(CERT_PATH, "ssl-cert-snakeoil.key")).read),
#   :SSLCertName        => [[ "CN",WEBrick::Utils::getservername ]],
#   :app                => App
# }

# Rack::Server.start webrick_options

# require 'sinatra/base'
# require 'openssl'
# require 'webrick'
# require 'webrick/https'

# class App1 < Sinatra::Base
#   get '/' do
#     'app1'
#   end
# end

# class App2 < Sinatra::Base
#   get '/' do
#     'app2'
#   end
# end

# app = Rack::Builder.new do 
#   map '/app1' do
#     run App1
#   end
#   map '/app2' do
#     run App2
#   end
# end

# webrick_options = {
#   :Port               => 8444,
#   :Logger             => WEBrick::Log::new($stdout, WEBrick::Log::DEBUG),
#   :DocumentRoot       => "./public",
#   :SSLEnable          => true,
#   :SSLCertificate     => OpenSSL::X509::Certificate.new(  File.open("/opt/sinatra-ebay-cert/ssl-cert-snakeoil.pem").read),
#   :SSLPrivateKey      => OpenSSL::PKey::RSA.new(          File.open("/opt/sinatra-ebay-cert/ssl-cert-snakeoil.key").read),
#   :SSLCertName        => [ [ "CN",WEBrick::Utils::getservername ] ],
#   :app                => app
# }

# #Rack::Handler::WEBrick.run app, webrick_options
# Rack::Server.start webrick_options