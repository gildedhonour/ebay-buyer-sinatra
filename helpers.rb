module Sinatra
  module UserHelper
    def current_user
      return nil unless session[:user_id]
      @current_user ||= User.where(id: session[:user_id]).first
    end
    
    def logged_in?
      !!self.current_user
    end
    
    def authenticate!
      redirect "/login?return_url=#{request.fullpath}" unless self.logged_in?
    end

    def current_ebay_accounts
      return nil unless current_user
      return current_user.ebay_accounts.first.map{|x| {x[:id]=> x[:user_name]}} unless session[:current_user_ebay_accounts]
      session[:current_user_ebay_accounts]
    end

    def current_ebay_accounts=(account_ids)
      raise "The user is not authenticated." unless current_user
      accounts = current_user.ebay_accounts.where(:id.in => account_ids)
      session[:current_user_ebay_accounts] = accounts.map{|x| {x[:id] => x[:user_name]}}
    end

  end
  
  module ViewHelper
    def pjax_request?
      !!env['HTTP_X_PJAX']
    end

    def ajax_request?
      env["HTTP_X_REQUESTED_WITH"] == "XMLHttpRequest"
    end

    def show_layout?
      !(pjax_request? or ajax_request?)
    end

    def menu_item(name, url, options = {})
      icon = options.fetch(:icon){ false }
      icon_class = options.fetch(:icon_class){ [] }
      classes = options.fetch(:classes){ [] }
      
      classes << "menu-item"
      classes << name.downcase.gsub(" ", "_")
      classes << (request.path_info == url ? "active" : "")  
      "<li class='#{classes.join(" ")}'>
        <a href='#{url}'>" +
        (icon ? "<i class='#{icon_class}'></i>" : "") + 
        "#{name}</a>
      </li>"
    end
  end

  helpers UserHelper
  helpers ViewHelper
end