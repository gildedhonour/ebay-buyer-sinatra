class EbayItem
  include Mongoid::Document
  include Mongoid::Timestamps
  
  FIRST_PAGE_INDEX = 1 
  PAGE_SIZE = 2 

  field :item_id, :type => Integer
  field :title, :type => String 
  field :subtitle, :type => String 
  field :description, :type => String
  field :condition_description, :type => String
  field :view_item_url, :type => String
  field :picture_urls, :type => Array
  field :gallery_url, :type => String
  field :country, :type => String
  field :location, :type => String
  field :postal_code, :type => Integer
  field :currency_id, :type => String
  field :converted_current_price, :type => Float
  field :current_price, :type => Float
  field :converted_start_price, :type => Float 
  field :start_price, :type => Float 
  field :time_left, :type => Time
  field :quantity_available, :type => Integer
  field :quantity, :type => Integer
  field :quantity_sold, :type => Integer
  field :high_bidder_user_id, :type => String
  field :high_bidder_feedback_score, :type => Integer
  field :high_bidder_feedback_private, :type => Boolean
  field :high_bidder_feedback_rating_star, :type => String
  field :reserve_met, :type => Boolean 
  field :bid_count, :type => Integer
  field :buyitnow_price, :type => Float 
  field :converted_buyitnow_price, :type => Float 
  field :ship_to_locations, :type => Array
  field :site, :type => String
  field :local_pickup, :type => Boolean
  field :shipping_service_cost, :type => Float 
  field :shipping_type, :type => String
  field :listed_shipping_service_cost, :type => Float 
  field :global_shipping, :type => Boolean
  field :handling_time, :type => Integer
  field :end_time, :type => Time 
  field :start_time, :type => Time
  field :time_left, :type => String
  field :payment_methods, :type => Array
  field :auto_pay, :type => Boolean
  field :best_offer_enabled, :type => Boolean
  field :buyitnow_available, :type => Boolean
  field :listing_type, :type => String
  field :listing_status, :type => String
  field :return_description, :type => String
  field :return_refund, :type => String
  field :returns_accepted, :type => String
  field :returns_within, :type => String
  field :return_shipping_cost_paid_by, :type => String
  field :seller_feedback_score, :type => Integer
  field :seller_feedback_percent, :type => Float
  field :seller_feedback_rating_star, :type => String
  field :seller_user_id, :type => String
  field :top_rated_seller, :type => Boolean 
  field :hit_count, :type => Integer

  belongs_to :best_offer_ebay_item
  belongs_to :lost_ebay_item
  belongs_to :watch_ebay_item
  belongs_to :won_ebay_item
  belongs_to :bid_ebay_item
  belongs_to :primary_ebay_category,    :class_name => "EbayCategory", :inverse_of => "primary_ebay_items"
  belongs_to :secondary_ebay_category,  :class_name => "EbayCategory", :inverse_of => "secondary_ebay_items"
  belongs_to :ebay_condition
  belongs_to :ebay_product

  validates_uniqueness_of :item_id, :message => "The eBay item #{:item_id} already exists"

  def total_current_price
    current_price + shipping_service_cost
  end

  def shipping_service_display_cost
    shipping_service_cost == 0 ? "Free" : "#{shipping_service_cost}"
  end
end