class EbayAccount
  include Mongoid::Document
  include Mongoid::Timestamps

  field :user_name, :type => String
  field :token, :type => String
  field :token_expiration, :type =>Time
  field :best_offer_list_updated_at, :type => Time
  field :lost_list_updated_at, :type => Time
  field :watch_list_updated_at, :type => Time
  field :won_list_updated_at, :type => Time
  field :bid_list_updated_at, :type => Time

  has_and_belongs_to_many :user
  has_and_belongs_to_many :users_of_selected_ebay_accounts, class_name: "User", inverse_of: :selected_ebay_accounts
  
  has_many :best_offer_ebay_items
  has_many :lost_ebay_items
  has_many :watch_ebay_items
  has_many :won_ebay_items
  has_many :bid_ebay_items

  #must a list be updated?
  #it depends on the time when it has been updated previously
  def has_to_update_list?(list_type)
    EbayAccount::validate_list_type!(list_type)
    min_time_to_update = 60*60
    list_method_name = "#{list_type}_list_updated_at".to_sym()
    
    # if list_updated_at is nil or it has been update so long ago
    # then it should be updated
    !! (!self.send(list_method_name) or self.send(list_method_name) < Time.now - min_time_to_update)
  end

  #check if list type exists
  # if it doesn't then there will be an exception thrown
  def self.validate_list_type!(list_type)
    raise "List_type shoud be symbol"             unless list_type.is_a?(Symbol)
    raise "Unsupported list_type (#{list_type})"  unless [:best_offer, :lost, :watch, :won, :bid].include?(list_type)
  end

  # retrieve list items for ebay accounts
  # the items might be retrieved from ebay first and then saved in db
  def self.list_items_by_accounts(list_type, ebay_accounts, app_instance, show_flash_message = true)
    self.validate_list_type!(list_type)
    list_items = []
    ebay_api = Ebay::TradingApi.new
     # binding.pry
    ebay_accounts.each do |account|
      if account.has_to_update_list?(list_type)
        request_list_method_name = "request_#{list_type}_list".to_sym()
        list_items += ebay_api.send(request_list_method_name, account.token)[:value]
        if show_flash_message
          app_instance.send(:append_flash_message, :success, "Your #{list_type} List for account #{account.user_name} was fully updated.")
        end
      else
        list_method_name = "#{list_type}_ebay_items".to_sym()
        list_items += account.send(list_method_name)
        list_updated_at_method_name = "#{list_type}_list_updated_at".to_sym()
        list_updated_friendly_date = account.send(list_updated_at_method_name).ago_in_words #TODO!!!
        if show_flash_message
          app_instance.send(:append_flash_message, :success, "Your #{list_type} List for account #{account.user_name} was updated #{list_updated_friendly_date}.")
        end
      end
    end

    list_items
  end
end