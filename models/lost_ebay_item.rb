class LostEbayItem
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :created_date, :type => Time
  field :bid_assistant, :type => Boolean
  field :converted_max_bid, :type => Float
  field :max_bid, :type => Float
  field :quantity_bid, :type => Integer
  field :quantity_won, :type => Integer
  field :ebay_notes, :type => String
  field :private_notes, :type => String
  field :seller_email, :type => String
  field :selling_status, :type => String
  field :deleted_from_lost_list, :type => Boolean
  
  has_one :ebay_item, autosave: true
  belongs_to :ebay_account
  
end
