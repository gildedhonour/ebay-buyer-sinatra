class EbayCondition
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :name,    type: String
  field :site_id, type: Integer

  has_and_belongs_to_many :ebay_categories, :class_name => 'EbayCategory'
  has_one :ebay_item
end