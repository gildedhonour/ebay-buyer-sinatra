class BidEbayItem
  include Mongoid::Document
  include Mongoid::Timestamps
  
  has_one :ebay_item, autosave: true
  belongs_to :ebay_account
  
end