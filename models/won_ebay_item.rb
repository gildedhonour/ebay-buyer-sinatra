class WonEbayItem
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :order_id, :type => String 
  field :transaction_id, :type => String 
  field :order_line_item_id, :type => String 
  field :created_date, :type => Time 
  field :feedback_left_comment_type, :type => String
  field :feedback_received_comment_type, :type => String
  field :bid_assistant, :type => Boolean
  field :converted_max_bid, :type => Float
  field :max_bid, :type => Float
  field :quantity_bid, :type => Integer
  field :quantity_won, :type => Integer
  field :quantity_purchased, :type => Integer
  field :ebay_notes, :type => String
  field :private_notes, :type => String
  field :seller_email, :type => String
  field :paid_time, :type => Time
  field :paisa_pay_id, :type => String
  field :transaction_platform, :type => String
  field :transaction_status, :type => String
  field :payment_hold_status, :type => String
  field :buyer_paid_status, :type => String
  field :shipped_time, :type => Time
  field :deleted_from_won_list, :type => Boolean
  
  belongs_to :ebay_account
  has_one :ebay_item, autosave: true

  def payment_status
    payment_statuses = [
      {name: "Paid",     value: ["MarkedAsPaid", "Paid", "PaidCOD", "PaidWithEscrow", "PaidWithPaisaPay", "PaidWithPaisaPayEscrow", "PaidWithPayPal"]},
      {name: "Pending",  value: ["PaymentPending", "PaymentPendingWithEscrow", "PaymentPendingWithPaisaPay", "PaymentPendingWithPaisaPayEscrow", "PaymentPendingWithPayPal"]},
      {name: "Refunded", value: ["Refunded"]},
      {name: "Unpaid",   value: ["BuyerHasNotCompletedCheckout", "NotPaid", "PaisaPayNotPaid"]}
    ]
    if result = payment_statuses.find { |item| item[:value].include?(buyer_paid_status)}
      result[:name]
    else
      "Unknown"
    end
  end

  def self.payment_statuses
    [{id: 1, value: "Paid"},
     {id: 2, value: "Pending"},
     {id: 3, value: "Refunded"},
     {id: 4, value: "Unpaid"},
     {id: 5, value: "Unknown"}]
  end

  def shipped?
    !!shipped_time
  end

  def self.shipment_statuses
    [{id: 1, value: "Shipped"},
     {id: 2, value: "Unshipped"}]
  end
end