class WatchEbayItem
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :ebay_notes, :type => String
  field :private_notes, :type => String
  field :deleted_from_watch_list, :type => Boolean
  
  has_one :ebay_item #, autosave: true
  belongs_to :ebay_account
  
end
