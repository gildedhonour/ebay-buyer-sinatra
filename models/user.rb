require 'bcrypt'
require 'digest/md5'
require 'openssl'

class User
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :user_name, :type => String
  field :email, :type => String#, :required => true
  field :email_confirmed, :type => Boolean, :default => false
  field :perishable_token, :type => String
  field :hashed_password,  :type => String
  field :password_reset_sent,  :type => DateTime, :default => nil

  has_and_belongs_to_many :ebay_accounts
  has_and_belongs_to_many :selected_ebay_accounts, class_name: "EbayAccount", inverse_of: :users_of_selected_ebay_accounts

  validates_uniqueness_of :email, :case_sensitive => false, :message => "That email address is already being used"
  validates_uniqueness_of :user_name, :allow_nil => :true, :case_sensitive => false, :message => "That user_name is already being used"
  validates_length_of :user_name, :maximum => 20, :message => "Your user_name must be 15 characters or fewer"
  validate :malformed_user_name

  after_create :finalize
  MIN_PASSWORD_LENGTH = 6

  def finalize
    set_user_name
    set_perishable_token
  end

  # Generate a multi-use token for account confirmation and password resets
  def set_perishable_token
    self.perishable_token = Digest::MD5.hexdigest( rand.to_s )
    save
  end

  # Reset the perishable token
  def reset_perishable_token
    self.perishable_token = nil
    save
  end

  # Store the hash of the password
  def password=(pass)
    self.hashed_password = BCrypt::Password.create(pass, :cost => 10)
  end

  # Create a new perishable token and set the date the password reset token was
  # sent so tokens can be expired after 2 days
  def set_password_reset_token
    self.password_reset_sent = DateTime.now
    set_perishable_token
    self.perishable_token
  end

  # Set a new password, clear the date the password reset token was sent and
  # reset the perishable token
  def reset_password(pass)
    self.password = pass
    self.password_reset_sent = nil
    reset_perishable_token
  end

  # Authenticate the user by checking their credentials
  def self.authenticate(email, pass)
    user = User.where(email: email).first
    return nil unless user
    return user if BCrypt::Password.new(user.hashed_password) == pass
    nil
  end

  # Reset the user_name to one given
  def reset_email(params)
    self.email = params[:email]
    return false unless save
  end

  def set_user_name
    chars = ('0'..'9').to_a
    user_name = self.email.split("@").at(0)  #TODO IF OVER 15 chars break there
    
    # Until retreiving a user_name returns false, generate a new user_name
    until User.where(user_name: user_name).first.nil?
      user_name = self.email.split("@").at(0) + (0...3).map{ chars[rand(10)] }.join
    end

    self.user_name = user_name
  end

  def has_ebay_accounts?
    self.ebay_accounts.any?
  end
  
  def self.valid_password?(password)
    password.length >= self::MIN_PASSWORD_LENGTH
  end

  private

  def malformed_user_name
    unless (user_name =~ /[@!"#$\%&'()*,^~{}|`=:;\\\/\[\]\s?]/).nil? && (user_name =~ /^[.]/).nil? && (user_name =~ /[.]$/).nil? && (user_name =~ /[.]{2,}/).nil?
      errors.add(:user_name, "The user_name contains restricted characters. Try sticking to letters, numbers, hyphens and underscores.")
    end
  end
end