class EbayCategory
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :name, :type => String
  field :site_id, :type => Integer
  field :localization_id, :type => Integer

  has_many :ebay_conditions
  has_many :primary_ebay_items, :class_name => "EbayItem", :inverse_of => :primary_ebay_category
  has_many :secondary_ebay_items, :class_name => "EbayItem", :inverse_of => :secondary_ebay_category
  has_many :ebay_products
end