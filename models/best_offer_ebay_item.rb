class BestOfferEbayItem
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :best_offer, :type => Float
  field :best_offer_status, :type => String  # Accepted, Active, AdminEnded, Countered, CustomCode, Declined, Expired, Pending, Retracted
  field :ebay_notes, :type => String
  field :private_notes, :type => String
  
  has_one :ebay_item #, autosave: true
  belongs_to :ebay_account
  
end