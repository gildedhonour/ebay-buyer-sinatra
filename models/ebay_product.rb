class EbayProduct
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :name, :type => String
  field :site_id, :type => Integer

  belongs_to :ebay_category
end