class String
  def nil_or_empty?
    nil? || self.strip().empty?
  end
end