require_relative './string_extensions'
module URI
  # is uri local meaning without a domain name specified
  def local?
    (!self.scheme or self.scheme.nil_or_empty?) and
    (!self.host or self.host.nil_or_empty?)and 
    (!self.hostname or self.hostname.nil_or_empty?) and 
    (!self.port or self.port.nil_or_empty?) and
    (!!self.path or !self.path.nil_or_empty?)
  end
end