class Notifier
  def self.send_forgot_password_notification(recipient, token)
    Pony.mail(:to => recipient,
              :subject => "Reset your app.com password",
              :from => "app@app.com",
              :headers => { 'Content-Type' => 'text/html' },
              :body => render_haml_template("forgot_password", {:token => token}),
              :via => :smtp, 
              :via_options => App::PONY_VIA_OPTIONS)
  end

  def self.send_signup_notification(recipient, token)
    Pony.mail(:to => recipient,
              :subject => "Activate your account on App.com",
              :from => "app@app.com",
              :headers => { 'Content-Type' => 'text/html' },
              :body => render_haml_template("activate_account", {:token => token}),
              :via => :smtp, 
              :via_options => App::PONY_VIA_OPTIONS)
  end

 
  private

  # Initiate Haml Engine to send emails
  def self.render_haml_template(template, opts)
    engine = Haml::Engine.new(File.open("views/notifier/#{template}.haml", "rb").read)
    engine.render(Object.new, opts)
  end
end
