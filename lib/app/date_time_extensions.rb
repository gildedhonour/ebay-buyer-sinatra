module DateTimeExtensions
  # date-time in the format of "7 Feb 2013 12:02:48PM" 
  def pretty
    self.strftime("%e %b %Y %H:%m:%S%p")
  end
end

class Time
  include DateTimeExtensions
end

class DateTime
  include DateTimeExtensions
end