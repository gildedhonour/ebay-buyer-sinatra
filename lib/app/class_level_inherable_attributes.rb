# it allows to use super classes instance attributes in
# subclasses with their default values set in a super class
# For instance:
# 
# class Base1
#   @base_var = 123
#   class << self
#     attr_accessor :base_var
#   end
# end
# puts("1 - #{Base1.base_var}") # => 123

# class Child1 < Base1
# end
# puts("2 - #{Child1.base_var}") # => nil

# class Base2
#   include ClassLevelInheritableAttributes
#   inheritable_attributes :base_var
#   @base_var = 12345
# end
# puts("3 - #{Base2.base_var}") # => 12345

# class Child2 < Base2
# end
# puts("4 - #{Child2.base_var}") # => 12345

module ClassLevelInheritableAttributes
  def self.included(base)
    base.extend(ClassMethods)
  end

  module ClassMethods
    def inheritable_attributes(*args)
      @inheritable_attributes ||= [:inheritable_attributes]
      @inheritable_attributes += args
      args.each do |arg| 
        class_eval <<-EVAL
            class << self
              attr_accessor :#{arg} 
            end
        EVAL
      end

      @inheritable_attributes
    end

    def inherited(subclass)
      @inheritable_attributes.each do |item|
        instance_var = "@#{item}"
        subclass.instance_variable_set(instance_var, instance_variable_get(instance_var))
      end
    end

  end
end