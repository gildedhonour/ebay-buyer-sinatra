module Ebay
  class TradingApi < Api 
    def self.generate_login_url(session_id)
      base_url = sandbox ? sandbox_login_url : production_login_url
      "#{base_url}?SignIn&RuName=#{Api::ru_name}&SessID=#{session_id}"
    end

    def request_auth_token_id(session_id)
      body = "<SessionID>#{session_id}</SessionID>"
      generic_request("FetchToken", body) do |document|
        auth_token_id = document.search("eBayAuthToken").text()
        exp_date = document.search("HardExpirationTime").text()
        {token: auth_token_id, expiration_date: exp_date}
      end
    end

    def request_session_id
      body = "<RuName>#{Api::ru_name}</RuName>"
      generic_request("GetSessionID", body) do |document|
        document.search("SessionID").text()
      end
    end
        
    def request_won_list(auth_token)
      # return request_generic_list(auth_token, :won)

       #TODO - remove! paging should be done by retrieving from db
      page = {}
      page[:index]  ||= 1
      page[:size]   ||= 3
      body = <<-BODY 
          <RequesterCredentials>
            <eBayAuthToken>#{auth_token}</eBayAuthToken>
          </RequesterCredentials>
          <BuyingSummary>
            <Include>true</Include>
          </BuyingSummary>
          <DeletedFromWonList>
            <Include>true</Include>
            <IncludeNotes>true</IncludeNotes>
            <Pagination>
              <EntriesPerPage>#{page[:size]}</EntriesPerPage>
              <PageNumber>#{page[:index]}</PageNumber>
            </Pagination>
          </DeletedFromWonList>
          <WonList>
            <Include>true</Include>
            <IncludeNotes>true</IncludeNotes>
            <Pagination>
              <EntriesPerPage>#{page[:size]}</EntriesPerPage>
              <PageNumber>#{page[:index]}</PageNumber>
            </Pagination>
          </WonList>
        BODY

      generic_request("GetMyeBayBuying", body) do |document|
        ebay_account = EbayAccount.where(token: auth_token).first
        won_ebay_items = []
        ["WonList", "DeletedFromWonList"].each do |won_list_node|
          won_list_items_xml = document.xpath("//ebay:#{won_list_node}//ebay:Transaction", {"ebay" => Api::xml_namespace()})
          won_list_items_xml.each do |node|
            ebay_item_hash = {}
            ebay_item_hash[:item_id] = get_node_text(node, "Item/ItemID") 
            ebay_item_hash[:gallery_url] = get_node_text(node, "Item/PictureDetails/GalleryURL")
            ebay_item_hash[:view_item_url] = get_node_text(node,"Item/ListingDetails/ViewItemURL")
            ebay_item_hash[:converted_start_price] = get_node_text(node,"Item/ListingDetails/ConvertedStartPrice")
            ebay_item_hash[:end_time] = get_node_text(node,"Item/ListingDetails/EndTime")
            ebay_item_hash[:start_time] = get_node_text(node,"Item/ListingDetails/StartTime")
            ebay_item_hash[:listing_type] = get_node_text(node,"Item/ListingType")
            ebay_item_hash[:seller_user_id] = get_node_text(node, "Item/Seller/UserID")
            ebay_item_hash[:current_price] = get_node_text(node,"Item/SellingStatus/CurrentPrice")
            ebay_item_hash[:bid_count] = get_node_text(node,"Item/SellingStatus/BidCount")
            ebay_item_hash[:converted_current_price] = get_node_text(node,"Item/SellingStatus/ConvertedCurrentPrice")
            ebay_item_hash[:shipping_service_cost] = get_node_text(node, "Item/ShippingDetails/ShippingServiceOptions/ShippingServiceCost") 
            ebay_item_hash[:local_pickup] = get_node_text(node, "Item/ShippingDetails/ShippingServiceOptions/LocalPickup")
            ebay_item_hash[:shipping_type] = get_node_text(node, "Item/ShippingDetails/ShippingType") 
            ebay_item_hash[:site] = get_node_text(node,"Item/Site")
            ebay_item_hash[:start_price] = get_node_text(node,"Item/StartPrice")
            ebay_item_hash[:sub_title] = get_node_text(node,"Item/SubTitle")
            ebay_item_hash[:title] = get_node_text(node,"Item/Title")
            
            ebay_item = EbayItem.where(item_id: ebay_item_hash[:item_id]).first
            ebay_item = EbayItem.create!(ebay_item_hash) unless ebay_item
                      
            won_ebay_item_hash = {}
            won_ebay_item_hash[:order_id] = get_node_text(node, "OrderID")
            won_ebay_item_hash[:transaction_id] = get_node_text(node, "TransactionID")
            won_ebay_item_hash[:order_line_item_id] = get_node_text(node, "OrderLineItemID")
            won_ebay_item_hash[:created_date] = get_node_text(node, "CreatedDate")
            won_ebay_item_hash[:feedback_left_comment_type] = get_node_text(node, "FeedbackLeft/CommentType")
            won_ebay_item_hash[:feedback_received_comment_type] = get_node_text(node, "FeedbackReceived/CommentType")
            won_ebay_item_hash[:bid_assistant] = get_node_text(node, "Item/BiddingDetails/BidAssistant")
            won_ebay_item_hash[:converted_max_bid] = get_node_text(node, "Item/BiddingDetails/ConvertedMaxBid")
            won_ebay_item_hash[:max_bid] = get_node_text(node, "Item/BiddingDetails/MaxBid")
            won_ebay_item_hash[:quantity_bid] = get_node_text(node, "Item/BiddingDetails/QuantityBid")
            won_ebay_item_hash[:quantity_won] = get_node_text(node, "Item/BiddingDetails/QuantityWon")
            won_ebay_item_hash[:quantity_purchased] = get_node_text(node, "QuantityPurchased")
            won_ebay_item_hash[:ebay_notes] = get_node_text(node, "Item/eBayNotes")
            won_ebay_item_hash[:private_notes] = get_node_text(node, "Item/PrivateNotes")
            won_ebay_item_hash[:seller_email] = get_node_text(node, "Item/Seller/Email")
            won_ebay_item_hash[:paid_time] = get_node_text(node, "PaidTime")
            won_ebay_item_hash[:paisa_pay_id] = get_node_text(node, "PaisaPayID")
            won_ebay_item_hash[:transaction_platform] = get_node_text(node, "Platform")
            won_ebay_item_hash[:payment_hold_status] = get_node_text(node, "Status/PaymentHoldStatus")
            won_ebay_item_hash[:buyer_paid_status] = get_node_text(node, "BuyerPaidStatus")
            won_ebay_item_hash[:shipped_time] = get_node_text(node, "ShippedTime")
            won_ebay_item_hash[:deleted_from_won_list] = won_list_node.include?("Deleted")
              
            won_ebay_item = WonEbayItem.where(:ebay_item_id => ebay_item.id, :ebay_account_id => ebay_account.id).first
            unless won_ebay_item
              won_ebay_item_hash[:ebay_item_id] = ebay_item.id
              won_ebay_item = WonEbayItem.create!(won_ebay_item_hash)
              won_ebay_item.ebay_item = ebay_item
              won_ebay_item.ebay_account = ebay_account
              won_ebay_item.save!
            else
              won_ebay_item.update_attributes(won_ebay_item_hash)
            end

            won_ebay_items <<  won_ebay_item
          end
         end

         ebay_account.won_list_updated_at = Time.now
         ebay_account.save!
         won_ebay_items
      end
    end
    
    def request_watch_list(auth_token)
       #TODO - remove! paging should be done by retrieving from db
      page = {}
      page[:index]  ||= 1
      page[:size]   ||= 3
      body = <<-BODY 
          <RequesterCredentials>
            <eBayAuthToken>#{auth_token}</eBayAuthToken>
          </RequesterCredentials>
          <WatchList>
            <Include>true</Include>
            <IncludeNotes>true</IncludeNotes>
            <Pagination>
              <EntriesPerPage>#{page[:size]}</EntriesPerPage>
              <PageNumber>#{page[:index]}</PageNumber>
            </Pagination>
          </WatchList>
        BODY

      generic_request("GetMyeBayBuying", body) do |document|
        ebay_account = EbayAccount.where(:token => auth_token).first
        watch_ebay_items = []
        watch_ebay_items_xml = document.xpath("//ebay:WatchList//ebay:Item", {"ebay" => Api::xml_namespace()})
        watch_ebay_items_xml.each do |node|
          ebay_item_hash = {}
          ebay_item_hash[:buyitnow_price] = get_node_text(node, "BuyItNowPrice") 
          ebay_item_hash[:item_id] = get_node_text(node, "ItemID") 
          ebay_item_hash[:gallery_url] = get_node_text(node, "PictureDetails/GalleryURL")
          ebay_item_hash[:view_item_url] = get_node_text(node,"ListingDetails/ViewItemURL")          
          ebay_item_hash[:converted_buyitnow_price] = get_node_text(node,"ListingDetails/ConvertedBuyItNowPrice")
          ebay_item_hash[:converted_start_price] = get_node_text(node,"ListingDetails/ConvertedStartPrice")
          ebay_item_hash[:end_time] = get_node_text(node,"ListingDetails/EndTime")
          ebay_item_hash[:start_time] = get_node_text(node,"ListingDetails/StartTime")
          ebay_item_hash[:listing_type] = get_node_text(node,"ListingType")
          ebay_item_hash[:quantity_available] = get_node_text(node,"QuantityAvailable")
          ebay_item_hash[:seller_user_id] = get_node_text(node, "Seller/UserID")
          ebay_item_hash[:seller_feedback_score] = get_node_text(node, "Seller/FeedbackScore")
          ebay_item_hash[:seller_feedback_rating_star] = get_node_text(node, "Seller/FeedbackRatingStar")
          ebay_item_hash[:current_price] = get_node_text(node,"SellingStatus/CurrentPrice")
          ebay_item_hash[:bid_count] = get_node_text(node,"SellingStatus/BidCount")
          ebay_item_hash[:converted_current_price] = get_node_text(node,"SellingStatus/ConvertedCurrentPrice")
          ebay_item_hash[:high_bidder_user_id] = get_node_text(node,"SellingStatus/HighBidder/UserID")
          ebay_item_hash[:high_bidder_feedback_score] = get_node_text(node,"SellingStatus/HighBidder/FeedbackScore")
          ebay_item_hash[:high_bidder_feedback_rating_star] = get_node_text(node,"SellingStatus/HighBidder/FeedbackRatingStar")
          ebay_item_hash[:shipping_service_cost] = get_node_text(node, "ShippingDetails/ShippingServiceOptions/ShippingServiceCost") 
          ebay_item_hash[:local_pickup] = get_node_text(node, "ShippingDetails/ShippingServiceOptions/LocalPickup")
          ebay_item_hash[:shipping_type] = get_node_text(node, "ShippingDetails/ShippingType")
          ebay_item_hash[:site] = get_node_text(node,"Site")
          ebay_item_hash[:start_price] = get_node_text(node,"StartPrice")
          ebay_item_hash[:sub_title] = get_node_text(node,"SubTitle")
          ebay_item_hash[:title] = get_node_text(node,"Title")
          ebay_item_hash[:time_left] = get_node_text(node,"TimeLeft")

          #TODO
          ebay_item = EbayItem.where(item_id: ebay_item_hash[:item_id]).first
          # ebay_item = ebay_item ? EbayItem.create!(ebay_item_hash) : ebay_item.update_attributes(ebay_item_hash)
          unless ebay_item
              ebay_item = EbayItem.create!(ebay_item_hash)
          end

          watch_ebay_item_hash = {}
          watch_ebay_item_hash[:ebay_notes] = get_node_text(node, "eBayNotes")
          watch_ebay_item_hash[:private_notes] = get_node_text(node, "PrivateNotes")

          watch_ebay_item = WatchEbayItem.where(ebay_item_id: ebay_item.id, ebay_account_id: ebay_account.id).first
          unless watch_ebay_item
            watch_ebay_item_hash[:ebay_item_id] = ebay_item.id
            watch_ebay_item = WatchEbayItem.create!(watch_ebay_item_hash)
            watch_ebay_item.ebay_item = ebay_item
            watch_ebay_item.ebay_account = ebay_account
          else
            watch_ebay_item.update_attributes(watch_ebay_item_hash)
          end
                  
          watch_ebay_item.save!
          watch_ebay_items <<  watch_ebay_item
        end
        
        ebay_account.watch_list_updated_at = Time.now
        ebay_account.save! 
        watch_ebay_items
      end
    end
    
    def request_best_offer_list(auth_token)
      #TODO - remove! paging should be done by retrieving from db
      page = {}
      page[:index]  ||= 1
      page[:size]   ||= 3
      body = <<-BODY 
          <RequesterCredentials>
            <eBayAuthToken>#{auth_token}</eBayAuthToken>
          </RequesterCredentials>
          <BestOfferList>
            <Include>true</Include>
            <IncludeNotes>true</IncludeNotes>
            <Pagination>
              <EntriesPerPage>#{page[:size]}</EntriesPerPage>
              <PageNumber>#{page[:index]}</PageNumber>
            </Pagination>
          </BestOfferList>
        BODY

      generic_request("GetMyeBayBuying", body) do |document|
        ebay_account = EbayAccount.where(:token => auth_token).first
        best_offer_ebay_items = []
        best_offer_ebay_items_xml = document.xpath("//ebay:BestOfferList//ebay:Item", {"ebay" => Api::xml_namespace()})
        best_offer_ebay_items_xml.each do |node|
          ebay_item_hash = {}
          ebay_item_hash[:buyitnow_price] = get_node_text(node, "BuyItNowPrice") 
          ebay_item_hash[:item_id] = get_node_text(node, "ItemID") 
          ebay_item_hash[:gallery_url] = get_node_text(node, "PictureDetails/GalleryURL")
          ebay_item_hash[:view_item_url] = get_node_text(node,"ListingDetails/ViewItemURL")          
          ebay_item_hash[:converted_buyitnow_price] = get_node_text(node,"ListingDetails/ConvertedBuyItNowPrice")
          ebay_item_hash[:start_time] = get_node_text(node,"ListingDetails/StartTime")
          ebay_item_hash[:seller_user_id] = get_node_text(node, "Seller/UserID")
          ebay_item_hash[:seller_feedback_score] = get_node_text(node, "Seller/FeedbackScore")
          ebay_item_hash[:seller_feedback_rating_star] = get_node_text(node, "Seller/FeedbackRatingStar")
          ebay_item_hash[:current_price] = get_node_text(node,"SellingStatus/CurrentPrice")
          ebay_item_hash[:converted_current_price] = get_node_text(node,"SellingStatus/ConvertedCurrentPrice")
          ebay_item_hash[:shipping_service_cost] = get_node_text(node, "ShippingDetails/ShippingServiceOptions/ShippingServiceCost") 
          ebay_item_hash[:local_pickup] = get_node_text(node, "ShippingDetails/ShippingServiceOptions/LocalPickup")
          ebay_item_hash[:shipping_type] = get_node_text(node, "ShippingDetails/ShippingType")
          ebay_item_hash[:site] = get_node_text(node,"Site")
          ebay_item_hash[:sub_title] = get_node_text(node,"SubTitle")
          ebay_item_hash[:title] = get_node_text(node,"Title")
          ebay_item_hash[:time_left] = get_node_text(node,"TimeLeft")
                    
          #TODO
          ebay_item = EbayItem.where(item_id: ebay_item_hash[:item_id]).first
          # ebay_item = ebay_item ? EbayItem.create!(ebay_item_hash) : ebay_item.update_attributes(ebay_item_hash)
          unless ebay_item
              ebay_item = EbayItem.create!(ebay_item_hash)
          end        
          
          best_offer_ebay_item_hash = {}
          best_offer_ebay_item_hash[:best_offer] = get_node_text(node, "BestOfferDetails/BestOffer")
          best_offer_ebay_item_hash[:best_offer_status] = get_node_text(node, "BestOfferDetails/BestOfferStatus")
          best_offer_ebay_item_hash[:ebay_notes] = get_node_text(node, "eBayNotes")
          best_offer_ebay_item_hash[:private_notes] = get_node_text(node, "PrivateNotes")
          best_offer_ebay_item = BestOfferEbayItem.where(:ebay_item_id => ebay_item.id, :ebay_account_id => ebay_account.id).first
          unless best_offer_ebay_item
            best_offer_ebay_item_hash[:ebay_item_id] = ebay_item.id
            best_offer_ebay_item = BestOfferEbayItem.create(best_offer_ebay_item_hash)
            best_offer_ebay_item.ebay_item = ebay_item
            best_offer_ebay_item.ebay_account = ebay_account
          else
            best_offer_ebay_item.update_attributes(best_offer_ebay_item_hash)
          end
          
          best_offer_ebay_item.save
          best_offer_ebay_items <<  best_offer_ebay_item
        end
        
        ebay_account.best_offer_list_updated_at = Time.now
        ebay_account.save!
        best_offer_ebay_items
      end
    end

    def request_bid_list(auth_token)
       #TODO - remove! paging should be done by retrieving from db
      page = {}
      page[:index]  ||= 1
      page[:size]   ||= 3
      body = <<-BODY 
          <RequesterCredentials>
            <eBayAuthToken>#{auth_token}</eBayAuthToken>
          </RequesterCredentials>
          <BidList>
            <Include>true</Include>
            <IncludeNotes>true</IncludeNotes>
            <Pagination>
              <EntriesPerPage>#{page[:size]}</EntriesPerPage>
              <PageNumber>#{page[:index]}</PageNumber>
            </Pagination>
          </BidList>
        BODY

      generic_request("GetMyeBayBuying", body) do |document|
        ebay_account = EbayAccount.where(token: auth_token).first
        bid_list_ebay_items = []
        bid_list_ebay_items_xml = document.xpath("//ebay:BidList//ebay:Item", {"ebay" => Api::xml_namespace()})
        bid_list_ebay_items_xml.each do |node|
          ebay_item_hash = {}
          ebay_item_hash[:buyitnow_price] = get_node_text(node, "BuyItNowPrice") 
          ebay_item_hash[:item_id] = get_node_text(node, "ItemID") 
          ebay_item_hash[:gallery_url] = get_node_text(node, "PictureDetails/GalleryURL")
          ebay_item_hash[:view_item_url] = get_node_text(node,"ListingDetails/ViewItemURL")          
          ebay_item_hash[:converted_buyitnow_price] = get_node_text(node,"ListingDetails/ConvertedBuyItNowPrice")
          ebay_item_hash[:start_time] = get_node_text(node,"ListingDetails/StartTime")
          ebay_item_hash[:seller_user_id] = get_node_text(node, "Seller/UserID")
          ebay_item_hash[:seller_feedback_score] = get_node_text(node, "Seller/FeedbackScore")
          ebay_item_hash[:seller_feedback_rating_star] = get_node_text(node, "Seller/FeedbackRatingStar")
          ebay_item_hash[:current_price] = get_node_text(node,"SellingStatus/CurrentPrice")
          ebay_item_hash[:converted_current_price] = get_node_text(node,"SellingStatus/ConvertedCurrentPrice")
          ebay_item_hash[:shipping_service_cost] = get_node_text(node, "ShippingDetails/ShippingServiceOptions/ShippingServiceCost") 
          ebay_item_hash[:local_pickup] = get_node_text(node, "ShippingDetails/ShippingServiceOptions/LocalPickup")
          ebay_item_hash[:shipping_type] = get_node_text(node, "ShippingDetails/ShippingType")
          ebay_item_hash[:site] = get_node_text(node,"Site")
          ebay_item_hash[:sub_title] = get_node_text(node,"SubTitle")
          ebay_item_hash[:title] = get_node_text(node,"Title")
          ebay_item_hash[:time_left] = get_node_text(node,"TimeLeft")
                     
          #TODO
          ebay_item = EbayItem.where(item_id: ebay_item_hash[:item_id]).first
          # ebay_item = ebay_item ? EbayItem.create!(ebay_item_hash) : ebay_item.update_attributes(ebay_item_hash)
          unless ebay_item
              ebay_item = EbayItem.create!(ebay_item_hash)
          end 
          
          bid_list_ebay_item_hash = {}
          bid_list_ebay_item_hash[:best_offer] = get_node_text(node, "BestOfferDetails/BestOffer")
          bid_list_ebay_item_hash[:best_offer_status] = get_node_text(node, "BestOfferDetails/BestOfferStatus")
          bid_list_ebay_item_hash[:ebay_notes] = get_node_text(node, "eBayNotes")
          bid_list_ebay_item_hash[:private_notes] = get_node_text(node, "PrivateNotes")
          bid_list_ebay_item = BidEbayItem.where(ebay_item_id: ebay_item.id, ebay_account_id: ebay_account.id).first
          
          unless bid_list_ebay_item
            bid_list_ebay_item_hash[:ebay_item_id] = ebay_item.id
            bid_list_ebay_item = BidEbayItem.create(bid_list_ebay_item_hash)
            bid_list_ebay_item.ebay_item = ebay_item
            bid_list_ebay_item.ebay_account = ebay_account
          else
            bid_list_ebay_item.update_attributes(bid_list_ebay_item_hash)
          end
          
          bid_list_ebay_item.save!
          bid_list_ebay_items <<  bid_list_ebay_item
        end
        
        ebay_account.bid_list_updated_at = Time.now
        ebay_account.save!
        bid_list_ebay_items
      end
    end
    
    def request_lost_list(auth_token)
      #TODO - remove! paging should be done by retrieving from db
      page = {}
      page[:index]  ||= 1
      page[:size]   ||= 3
      body = <<-BODY 
          <RequesterCredentials>
            <eBayAuthToken>#{auth_token}</eBayAuthToken>
          </RequesterCredentials>
          <LostList>
            <Include>true</Include>
            <IncludeNotes>true</IncludeNotes>
            <Pagination>
              <EntriesPerPage>#{page[:size]}</EntriesPerPage>
              <PageNumber>#{page[:index]}</PageNumber>
            </Pagination>
          </LostList>
        BODY

      generic_request("GetMyeBayBuying", body) do |document|
        ebay_account = EbayAccount.where(token: auth_token).first
        lost_list_ebay_items = []
        lost_list_ebay_items_xml = document.xpath("//ebay:LostList//ebay:Item", {"ebay" => Api::xml_namespace()})
        lost_list_ebay_items_xml.each do |node|
          ebay_item_hash = {}
          ebay_item_hash[:buyitnow_price] = get_node_text(node, "BuyItNowPrice") 
          ebay_item_hash[:item_id] = get_node_text(node, "ItemID") 
          ebay_item_hash[:gallery_url] = get_node_text(node, "PictureDetails/GalleryURL")
          ebay_item_hash[:view_item_url] = get_node_text(node,"ListingDetails/ViewItemURL")          
          ebay_item_hash[:converted_buyitnow_price] = get_node_text(node,"ListingDetails/ConvertedBuyItNowPrice")
          ebay_item_hash[:start_time] = get_node_text(node,"ListingDetails/StartTime")
          ebay_item_hash[:seller_user_id] = get_node_text(node, "Seller/UserID")
          ebay_item_hash[:seller_feedback_score] = get_node_text(node, "Seller/FeedbackScore")
          ebay_item_hash[:seller_feedback_rating_star] = get_node_text(node, "Seller/FeedbackRatingStar")
          ebay_item_hash[:current_price] = get_node_text(node,"SellingStatus/CurrentPrice")
          ebay_item_hash[:converted_current_price] = get_node_text(node,"SellingStatus/ConvertedCurrentPrice")
          ebay_item_hash[:shipping_service_cost] = get_node_text(node, "ShippingDetails/ShippingServiceOptions/ShippingServiceCost") 
          ebay_item_hash[:local_pickup] = get_node_text(node, "ShippingDetails/ShippingServiceOptions/LocalPickup")
          ebay_item_hash[:shipping_type] = get_node_text(node, "ShippingDetails/ShippingType")
          ebay_item_hash[:site] = get_node_text(node,"Site")
          ebay_item_hash[:sub_title] = get_node_text(node,"SubTitle")
          ebay_item_hash[:title] = get_node_text(node,"Title")
          ebay_item_hash[:time_left] = get_node_text(node,"TimeLeft")
                     
          #TODO
          ebay_item = EbayItem.where(item_id: ebay_item_hash[:item_id]).first
          # ebay_item = ebay_item ? EbayItem.create!(ebay_item_hash) : ebay_item.update_attributes(ebay_item_hash)
          unless ebay_item
              ebay_item = EbayItem.create!(ebay_item_hash)
          end 
           
          lost_list_ebay_item_hash = {}
          lost_list_ebay_item_hash[:best_offer] = get_node_text(node, "BestOfferDetails/BestOffer")
          lost_list_ebay_item_hash[:best_offer_status] = get_node_text(node, "BestOfferDetails/BestOfferStatus")
          lost_list_ebay_item_hash[:ebay_notes] = get_node_text(node, "eBayNotes")
          lost_list_ebay_item_hash[:private_notes] = get_node_text(node, "PrivateNotes")
         
          lost_list_ebay_item = LostEbayItem.where(ebay_item_id: ebay_item.id, ebay_account_id: ebay_account.id).first
          unless lost_list_ebay_item
            lost_list_ebay_item_hash[:ebay_item_id] = ebay_item.id
            lost_list_ebay_item = LostEbayItem.create(lost_list_ebay_item_hash)
            lost_list_ebay_item.ebay_item = ebay_item
            lost_list_ebay_item.ebay_account = ebay_account
          else
            lost_list_ebay_item.update_attributes(lost_list_ebay_item_hash)
          end
          
          lost_list_ebay_item.save!
          lost_list_ebay_items <<  lost_list_ebay_item
        end
        
        ebay_account.lost_list_updated_at = Time.now
        ebay_account.save!
        lost_list_ebay_items
      end
    end

    private

    def request_generic_list(auth_token, list_type)
      EbayAccount.validate_list_type!(list_type)
      
      #TODO - remove! paging should be done by retrieving from db
      page = {}
      page[:index]  ||= 1
      page[:size]   ||= 3
      list_name = list_type == :best_offer ? "BestOffer" : "#{list_type.to_s().capitalize()}"
      full_list_name = "#{list_name}List"
      body = <<-BODY 
          <RequesterCredentials>
            <eBayAuthToken>#{auth_token}</eBayAuthToken>
          </RequesterCredentials>
          <#{full_list_name}>
            <Include>true</Include>
            <IncludeNotes>true</IncludeNotes>
            <Pagination>
              <EntriesPerPage>#{page[:size]}</EntriesPerPage>
              <PageNumber>#{page[:index]}</PageNumber>
            </Pagination>
          </#{full_list_name}>
        BODY

      generic_request("GetMyeBayBuying", body) do |document|
        ebay_account = EbayAccount.where(token: auth_token).first
        generic_list_ebay_items = []
        generic_list_ebay_items_xml = document.xpath("//ebay:#{full_list_name}//ebay:Item", {"ebay" => Api::xml_namespace()})
        generic_list_ebay_items_xml.each do |node|
          ebay_item_hash = {}
          ebay_item_hash[:buyitnow_price] = get_node_text(node, "BuyItNowPrice") 
          ebay_item_hash[:item_id] = get_node_text(node, "ItemID") 
          ebay_item_hash[:gallery_url] = get_node_text(node, "PictureDetails/GalleryURL")
          ebay_item_hash[:view_item_url] = get_node_text(node,"ListingDetails/ViewItemURL")          
          ebay_item_hash[:converted_buyitnow_price] = get_node_text(node,"ListingDetails/ConvertedBuyItNowPrice")
          ebay_item_hash[:start_time] = get_node_text(node,"ListingDetails/StartTime")
          ebay_item_hash[:seller_user_id] = get_node_text(node, "Seller/UserID")
          ebay_item_hash[:seller_feedback_score] = get_node_text(node, "Seller/FeedbackScore")
          ebay_item_hash[:seller_feedback_rating_star] = get_node_text(node, "Seller/FeedbackRatingStar")
          ebay_item_hash[:current_price] = get_node_text(node,"SellingStatus/CurrentPrice")
          ebay_item_hash[:converted_current_price] = get_node_text(node,"SellingStatus/ConvertedCurrentPrice")
          ebay_item_hash[:shipping_service_cost] = get_node_text(node, "ShippingDetails/ShippingServiceOptions/ShippingServiceCost") 
          ebay_item_hash[:local_pickup] = get_node_text(node, "ShippingDetails/ShippingServiceOptions/LocalPickup")
          ebay_item_hash[:shipping_type] = get_node_text(node, "ShippingDetails/ShippingType")
          ebay_item_hash[:site] = get_node_text(node,"Site")
          ebay_item_hash[:sub_title] = get_node_text(node,"SubTitle")
          ebay_item_hash[:title] = get_node_text(node,"Title")
          ebay_item_hash[:time_left] = get_node_text(node,"TimeLeft")
                     
          ebay_item = EbayItem.where(item_id: ebay_item_hash[:item_id]).first
          ebay_item = EbayItem.create!(ebay_item_hash) unless ebay_item
          
          generic_list_ebay_item_hash = {}
          generic_list_ebay_item_hash[:best_offer] = get_node_text(node, "BestOfferDetails/BestOffer")
          generic_list_ebay_item_hash[:best_offer_status] = get_node_text(node, "BestOfferDetails/BestOfferStatus")
          generic_list_ebay_item_hash[:ebay_notes] = get_node_text(node, "eBayNotes")
          generic_list_ebay_item_hash[:private_notes] = get_node_text(node, "PrivateNotes")

          model_name = "#{list_name}EbayItem"
          generic_list_ebay_item_class = Object.const_get(model_name)
          generic_list_ebay_item = generic_list_ebay_item_class.where(ebay_item_id: ebay_item.id, ebay_account_id: ebay_account.id).first
          unless generic_list_ebay_item
            generic_list_ebay_item_hash[:ebay_item_id] = ebay_item.id
            generic_list_ebay_item = generic_list_ebay_item_class.create!(generic_list_ebay_item_hash)
            generic_list_ebay_item.ebay_item = ebay_item
            generic_list_ebay_item.ebay_account = ebay_account
            generic_list_ebay_item.save!
          else
            generic_list_ebay_item.update_attributes(generic_list_ebay_item_hash)
          end
          
          generic_list_ebay_items <<  generic_list_ebay_item
        end
        
      binding.pry   # !!!! DEBUG !!!!
        list_updated_at_hash = {"#{list_type}_list_updated_at" => Time.now}
        ebay_account.update_attributes(list_updated_at_hash)
        generic_list_ebay_items
      end
    end
  end
end