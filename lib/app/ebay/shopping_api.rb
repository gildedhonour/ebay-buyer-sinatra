module Ebay
  class ShoppingApi < Api 
    @production_api_url = "http://open.api.ebay.com/shopping?"
    @sandbox_api_url    = "http://open.api.sandbox.ebay.com/shopping?"

    def request_single_item(item_id)
      body = <<-BODY 
                <ItemID>#{item_id}</ItemID>
                <IncludeSelector>Description,Details</IncludeSelector>
              BODY

      generic_request("GetSingleItem", body, ShoppingApi::api_url) do |document|
        node = document.xpath("//ebay:Item", {"ebay" => Api::xml_namespace})
        ebay_item_hash = {}
        ebay_item_hash[:item_id] = get_node_text(node, "ItemID") 
        ebay_item_hash[:auto_pay] = get_node_text(node,"AutoPay")  
        ebay_item_hash[:bid_count] = get_node_text(node,"BidCount")
        ebay_item_hash[:best_offer_enabled] = get_node_text(node,"BestOfferEnabled")
        ebay_item_hash[:buyitnow_available] = get_node_text(node,"BuyItNowAvailable")
        ebay_item_hash[:buyitnow_price] = get_node_text(node,"BuyItNowPrice")
        ebay_item_hash[:condition_description] = get_node_text(node, "ConditionDescription")
        condition_name = get_node_text(node,"ConditionDisplayName")
        condition_id = get_node_text(node,"ConditionID")
        ebay_item_hash[:converted_current_price] = get_node_text(node,"ConvertedCurrentPrice")
        ebay_item_hash[:converted_current_price] = get_node_text(node,"ConvertedBuyItNowPrice")
        ebay_item_hash[:country] = get_node_text(node, "Country")
        ebay_item_hash[:current_price] = get_node_text(node,"CurrentPrice")
        ebay_item_hash[:description] = get_node_text(node, "Description") # Get the first Description
        ebay_item_hash[:end_time] = get_node_text(node,"EndTime")  
        ebay_item_hash[:gallery_url] = get_node_text(node, "GalleryURL")       
        ebay_item_hash[:global_shipping] = get_node_text(node,"GlobalShipping")
        ebay_item_hash[:handling_time] = get_node_text(node,"HandlingTime")
        ebay_item_hash[:high_bidder_user_id] = get_node_text(node,"HighBidder/UserID")
        ebay_item_hash[:high_bidder_feedback_score] = get_node_text(node,"HighBidder/FeedbackScore")
        ebay_item_hash[:high_bidder_feedback_private] = get_node_text(node,"HighBidder/FeedbackPrivate")
        ebay_item_hash[:high_bidder_feedback_rating_star] = get_node_text(node,"HighBidder/FeedbackRatingStar")
        ebay_item_hash[:hit_count] = get_node_text(node,"HitCount")
        ebay_item_hash[:listing_status] = get_node_text(node,"ListingStatus")
        ebay_item_hash[:listing_type] = get_node_text(node,"ListingType")  
        ebay_item_hash[:location] = get_node_text(node, "Location")
        
        ebay_item_hash[:payment_methods] = []
        payment_method_nodes = document.xpath("//ebay:Item//ebay:PaymentMethods", {"ebay" => Api::xml_namespace()})
        payment_method_nodes.each do |payment_method|
          ebay_item_hash[:payment_methods] << payment_method.text()
        end
        
        ebay_item_hash[:picture_urls] = []
        picture_url_nodes = document.xpath("//ebay:Item//ebay:PictureURL", {"ebay" => Api::xml_namespace()})
        picture_url_nodes.each do |picture_url|
          ebay_item_hash[:picture_urls] << picture_url.text()
        end
        
        ebay_item_hash[:postal_code] = get_node_text(node, "PostalCode")
        primary_category_id = get_node_text(node, "PrimaryCategoryID")
        primary_category_name = get_node_text(node, "PrimaryCategoryName")
        product_id = get_node_text(node, "ProductID")
        ebay_item_hash[:quantity] = get_node_text(node, "Quantity")
        ebay_item_hash[:quantity_sold] = get_node_text(node,"QuantitySold")
        ebay_item_hash[:reserve_met] = get_node_text(node,"ReserveMet")
        ebay_item_hash[:return_description] = get_node_text(node,"ReturnPolicy/Description")
        ebay_item_hash[:return_refund] = get_node_text(node,"ReturnPolicy/Refund")
        ebay_item_hash[:returns_accepted] = get_node_text(node,"ReturnPolicy/ReturnsAccepted")
        ebay_item_hash[:returns_within] = get_node_text(node,"ReturnPolicy/ReturnsWithin")
        ebay_item_hash[:return_shipping_cost_paid_by] = get_node_text(node,"ReturnPolicy/ShippingCostPaidBy")
        secondary_category_id = get_node_text(node, "SecondaryCategoryID")
        ebay_item_hash[:seller_user_id] = get_node_text(node,"Seller/UserID")  
        ebay_item_hash[:seller_feedback_rating_star] = get_node_text(node,"Seller/FeedbackRatingStar")
        ebay_item_hash[:seller_feedback_score] = get_node_text(node,"Seller/FeedbackScore")
        ebay_item_hash[:seller_feedback_percent] = get_node_text(node,"Seller/PositiveFeedbackPercent")
        ebay_item_hash[:top_rated_seller] = get_node_text(node,"Seller/TopRatedSeller")
        ebay_item_hash[:shipping_type] = get_node_text(node,"ShippingCostSummary/ShippingType")
        ebay_item_hash[:shipping_service_cost] = get_node_text(node,"ShippingCostSummary/ShippingServiceCost")
        ebay_item_hash[:listed_shipping_service_cost] = get_node_text(node,"ShippingCostSummary/ListedShippingServiceCost")
        ebay_item_hash[:start_time] = get_node_text(node,"StartTime")
        ebay_item_hash[:sub_title] = get_node_text(node,"SubTitle")
        ebay_item_hash[:time_left] = get_node_text(node,"TimeLeft")
        ebay_item_hash[:title] = get_node_text(node,"Title")
        ebay_item_hash[:site] = get_node_text(node,"Site")
        ebay_item_hash[:view_item_url] = get_node_text(node,"ViewItemURLForNaturalSearch")
        
        ebay_item_hash[:primary_ebay_category] = EbayCategory.where(:site_id => primary_category_id).first          
        unless ebay_item_hash[:primary_ebay_category]
          ebay_item_hash[:primary_ebay_category] = EbayCategory.create!(site_id: primary_category_id, name: primary_category_name)
        end
        
        #in XML response there might not be secondary category field
        #necessary to check if it's not nil
        if secondary_category_id
          ebay_item_hash[:secondary_ebay_category] = EbayCategory.where(:site_id => secondary_category_id).first
          unless ebay_item_hash[:secondary_ebay_category]
            ebay_item_hash[:secondary_ebay_category] = EbayCategory.create!(:site_id => secondary_category_id)
          end
        end

        ebay_item_hash[:ebay_condition] = EbayCondition.where(:site_id => condition_id).first
        unless ebay_item_hash[:ebay_condition]
          ebay_item_hash[:ebay_condition] = EbayCondition.create!(site_id: condition_id, name: condition_name)
        end

        ebay_item_hash[:ebay_product] = EbayProduct.where(:site_id => product_id).first
        unless ebay_item_hash[:ebay_product]
          ebay_item_hash[:ebay_product] = EbayProduct.create!(site_id: product_id)
        end

        ebay_item = EbayItem.where(item_id: ebay_item_hash[:item_id]).first
        unless ebay_item
          ebay_item = EbayItem.create!(ebay_item_hash)
        else
          ebay_item.update_attributes(ebay_item_hash)
        end

        ebay_item
      end
    end

    protected 

    def build_headers(api_method_name)
      {
      'X-EBAY-API-CALL-NAME' =>         api_method_name,
      'X-EBAY-API-APP-ID' =>            Api::app_id(),
      'X-EBAY-API-VERSION' =>           Api::version_number(),
      'X-EBAY-API-SITE-ID' =>           Api::site_id(),
      'X-EBAY-API-REQUEST-ENCODING' =>  "XML",
      'Content-Type' =>                 "text/xml"
      }
    end

    def build_http_https_request(uri, use_ssl = true)
      super(uri, false)
    end
  end
end