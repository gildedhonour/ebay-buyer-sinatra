require 'uri'
require 'net/http'
require 'nokogiri'
require 'pry' if ENV['RACK_ENV'] == 'development'
require_relative '../class_level_inherable_attributes'

module Ebay
  class Api 
    include ClassLevelInheritableAttributes
    inheritable_attributes  :production_api_url, 
                            :sandbox, 
                            :production_login_url,
                            :sandbox_api_url, 
                            :sandbox_login_url,
                            :site_id, 
                            :compatibility_level, 
                            :version_number, 
                            :xml_namespace

    @production_api_url   = "https://api.ebay.com/ws/api.dll"
    @sandbox              = false
    @production_login_url = "https://signin.ebay.com/ws/eBayISAPI.dll"
    @sandbox_api_url      = "https://api.sandbox.ebay.com/ws/api.dll"
    @sandbox_login_url    = "https://signin.sandbox.ebay.com/ws/eBayISAPI.dll"
    @site_id              = "0"
    @compatibility_level  = "795"
    @version_number       = "727"
    @xml_namespace        = "urn:ebay:apis:eBLBaseComponents"

    class << self
      attr_accessor :dev_id, :app_id, :cert_id, :ru_name 

      def configure
        yield(self) if block_given?
      end
     
      def api_url
        self.sandbox ? self.sandbox_api_url : self.production_api_url
      end
    end

    def initialize(debug_mode = false, request_timeout = 60)
      @debug_mode = debug_mode
      @request_timeout = request_timeout
    end

    def debug_mode?
      !!@debug_mode
    end

    def debug_mode=(value)
      @debug_mode = value
    end

    protected
    
    def build_http_https_request(uri, use_ssl = true)
      http_https = Net::HTTP.new(uri.host, uri.port)
      http_https.use_ssl = use_ssl
      http_https.set_debug_output($stderr) if debug_mode?
      http_https.open_timeout = @request_timeout
      http_https.read_timeout = @request_timeout
      http_https
    end

    def build_headers(api_method_name)
      {
        'X-EBAY-API-DEV-NAME' => Api::dev_id,
        'X-EBAY-API-APP-NAME' => Api::app_id,
        'X-EBAY-API-CERT-NAME' => Api::cert_id,
        'X-EBAY-API-COMPATIBILITY-LEVEL' => Api::compatibility_level,
        'X-EBAY-API-SITEID' => Api::site_id,
        'X-EBAY-API-CALL-NAME' => api_method_name,
        'Content-Type' => 'text/xml'
      }
    end

    def get_node_text(node, short_xpath)
      xpath = short_xpath.gsub(%r{[^/]+/?}) { |item| "ebay:#{item}" }
      node.xpath(xpath, {"ebay" => Api::xml_namespace}).text()
    end

    def generic_request(api_method_name, request_body, api_url = Api::api_url, &block)
      uri = URI(api_url)
      https = build_http_https_request(uri)
      headers = build_headers(api_method_name)
      body = <<-BODY 
        <?xml version="1.0" encoding="utf-8"?>
        <#{api_method_name}Request xmlns="#{Api::xml_namespace}">
             #{request_body}
        </#{api_method_name}Request>
        BODY

      response = https.post(uri.path, body, headers)
      document = Nokogiri.XML(response.body)
      if document.search("Ack").text().downcase()  == "success"
        result = yield(document)
        {is_success: true, value: result}
      else
        errors_xml = document.xpath("//ebay:Errors//ebay:ShortMessage", {"ebay" => Ebay::Api::xml_namespace()})
        {is_success: false, value: errors_xml.map { |item| item.text()} }
      end
    end
  end
end