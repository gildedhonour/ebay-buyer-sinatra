require 'bundler'
require 'pry' if ENV['RACK_ENV'] == 'development'
Bundler.require
require_relative "helpers"

class App < Sinatra::Base
    #group flash messages by its type and make 
    #each flash type will have the array of messages:
    #flash[:success]  = ["message1", "message2"]
    #flash[:error]    = ["message3", "message4"]
    after do
      @flash_messages ||= []
      @flash_messages.group_by {|x| x[:type]}.each do |item|
        flash[item[0]] = item[1].map{|x| x[:message]}
      end
    end
    
    protected
    
    #appends message to an array of flash messages
    #the accepted types are success, alert, notification, warning, error, information
    def append_flash_message(type, message)
      unless [:success, :alert, :notification, :warning, :error, :information].include?(type)
        raise "Unsupported type (#{type}) of the message"
      end

      @flash_messages ||= []
      @flash_messages << {type: type, message: message}
    end
end

require_relative "config/config"
Dir.glob("controllers/*.rb").each { |r| require_relative r}