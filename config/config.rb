require_relative '../lib/app/session'
require_relative '../lib/app/string_extensions'
require_relative '../lib/app/date_time_extensions'
require_relative '../lib/app/uri_extensions'
require_relative '../lib/app/ebay/api'
require_relative '../lib/app/ebay/shopping_api'
require_relative '../lib/app/ebay/trading_api'
require_relative '../models/all'

class App
  configure [:development, :test] { PONY_VIA_OPTIONS =  {} }
  configure :production do
    PONY_VIA_OPTIONS =  {
      :address        => "smtp.sendgrid.net",
      :port           => "25",
      :authentication => :plain,
      :user_name      => ENV['SENDGRID_USERNAME'],
      :password       => ENV['SENDGRID_PASSWORD'],
      :domain         => ENV['SENDGRID_DOMAIN']
    }
  end
  
  use Rack::Timeout
  Rack::Timeout.timeout = 10

  set :root, File.join(File.dirname(__FILE__), "..")
  set :haml, :escape_html => true
  set :logging, :dump_errors, :raise_errors
  set :sass, Compass.sass_engine_options
  set :sass, {:load_paths => ["#{App.root}/app/css", "#{Gem.loaded_specs['compass'].full_gem_path}/frameworks/compass/stylesheets"]}
  set :scss, sass

  register Sinatra::AssetPack
  register Sinatra::CompassSupport
  assets do
    serve '/js',      from: 'app/js' 
    serve '/css',     from: 'app/css'  
    serve '/images',  from: 'app/images'
    serve '/fonts',   from: 'app/fonts'
    js :main, ['/js/*.js']
    css :main, ['/css/*.css']
    js_compression  :jsmin     
    css_compression :simple   
  end

  set :method_override, true
  Mongoid.load!("config/mongoid.yml", :development)

  configure do   
    yaml = YAML.load_file("config/ebay.yml")[settings.environment.to_s]
    yaml.each_pair { |key, value| set(("ebay_"+ key).to_sym, value) }
    enable :sessions
    set :session_secret, "super secret user session"
    register Sinatra::Flash
  end 

  Ebay::Api.configure do |ebay|
    ebay.dev_id =   settings.ebay_dev_id
    ebay.app_id =   settings.ebay_app_id
    ebay.cert_id =  settings.ebay_cert_id
    ebay.ru_name =  settings.ebay_ru_name
  end

  Ebay::TradingApi.configure do |ebay|
    ebay.dev_id =   settings.ebay_dev_id
    ebay.app_id =   settings.ebay_app_id
    ebay.cert_id =  settings.ebay_cert_id
    ebay.ru_name =  settings.ebay_ru_name
  end

  helpers Sinatra::UserHelper
  helpers Sinatra::ViewHelper
  helpers Sinatra::ContentFor
  helpers do
      [:development, :production, :test].each do |environment|
        define_method ("#{environment.to_s}?") { settings.environment == environment }  
    end
  end

end