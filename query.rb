ENV['RACK_ENV'] ||= "development"
require File.dirname(__FILE__) + '/app'

start_time = Time.now

ebay_account = EbayAccount.where(:id => "50a1842aa01ae9fabe000002").first
#ebay_account.watch_list_updated_at = Time.now
#ebay_account.save

watch_list_data = Ebay::TradingApi.new().request_watch_list(ebay_account.token)
watch_ebay_items = watch_list_data[:is_success] ? watch_list_data[:value] : nil
puts watch_ebay_items
total_time = Time.now - start_time
puts "TOTAL LOOP TIME #{total_time}"