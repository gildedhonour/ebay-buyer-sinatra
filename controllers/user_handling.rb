class App
  # Allows a user to reset their username. Currently only allows users that
  # are not registered, users without a username and facebook users with the
  # screwed up username
  get '/reset-email' do
    unless current_user.nil? || current_user.email.empty? || current_user.email.match(/profile.php/)
      redirect "/"
    end

    haml :"login/reset_email" #, :layout => :'layout/simple'
  end

  post '/reset-email' do
    exists = User.where(:email => params[:email]).first
    if !params[:email].nil? && !params[:email].empty? && exists.nil?
      if current_user.reset_email(params)
        append_flash_message(:success, "Thank you for updating your email.")
      else
        append_flash_message(:success, "Your email could not be updated.")
      end
      redirect "/"
    else
      append_flash_message(:success, "Sorry, that email has already been taken or is not valid. Please try again.")
      haml :"login/reset_email"
    end
  end

  # Passwords can be reset by unauthenticated users by navigating to the forgot
  # password and page and submitting the email address they provided.
  get '/forgot_password' do
    haml :"login/forgot_password" #, :layout => :'layout/simple'
  end

  # We've got a pretty solid forgotten password implementation. It's simple:
  # the email address is looked up, if no user is found an error is provided.
  # If a user is found a token is generated and an email is sent to the user
  # with the url to reset their password. Users are then redirected to the
  # confirmation page to prevent repost issues
  post '/forgot_password' do
    user = User.where(:email => params[:email]).first
    unless user
      append_flash_message(:success, "Your account could not be found, please check your email and try again.")
      haml :"login/forgot_password"
    else
      Notifier.send_forgot_password_notification(user.email, user.set_password_reset_token)
      # Redirect to try to avoid repost issues
      session[:fp_email] = user.email
      redirect '/forgot_password_confirm'
    end
  end

  # Forgot password confirmation screen, displays email address that the email
  # was sent to
  get '/forgot_password_confirm' do
    @email = session.delete(:fp_email)
    haml :"login/forgot_password_confirm" #, :layout => :'layout/simple'
  end

  get '/reset_password' do
    redirect("/forgot_password", :layout => :'layout/simple') unless logged_in?
  end

  # Public reset password page, accessible via a valid token. Tokens are only
  # valid for 2 days and are unique to that user. The user is found using the
  # token and the reset password page is rendered
  get '/reset_password/:token' do
    user = User.where(:perishable_token => params[:token]).first
    if user.nil? || user.password_reset_sent.to_time < 2.days.ago
      append_flash_message(:success, "Your link is no longer valid, please request a new one.")
      redirect "/forgot_password"
    else
      @token = params[:token]
      @user = user
      haml :"login/password_reset" #, :layout => :'layout/simple'
    end
  end

  # Submitted passwords are checked for length and confirmation. If the user
  # does not have an email address they are required to provide one. Once the
  # password has been reset the user is redirected to /
  post '/reset_password' do
    user = nil
    if params[:token]
      user = User.where(:perishable_token => params[:token]).first
      if user and user.password_reset_sent.to_time < 2.days.ago
        user = nil
      end
    end

    unless user
      # XXX: yes, this is a code smell

      if params[:password].size == 0
        append_flash_message(:error, "Password must be present.")
        redirect "/reset_password/#{params[:token]}"
        return
      end

      if params[:password] != params[:password_confirm]
        append_flash_message(:error, "Passwords do not match.")
        redirect "/reset_password/#{params[:token]}"
        return
      end

      unless user.email
        unless params[:email]
          append_flash_message(:error, "Email must be provided.")
          redirect "/reset_password/#{params[:token]}"
          return
        else
          user.email = params[:email]
        end
      end

      user.password = params[:password]
      user.save!
      append_flash_message(:success, "Password successfully set.")
      redirect "/"
    else
      redirect "/forgot_password"
    end
  end

  get '/activate/:token' do
    user = User.where(perishable_token: params[:token]).first
    if user
      user.email_confirmed = true
      user.reset_perishable_token
      user.save!
      session[:user_id] = user.id
      append_flash_message(:success, "Your app.com account has been activated.")
    else
      append_flash_message(:error, "Can't find User Account for this link.")
    end

    redirect "/"
  end
end
