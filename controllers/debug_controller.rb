class App
  get '/debug/clear-session' do
    session.clear
    redirect '/'
  end

  get '/debug/test' do
    append_flash_message(:notification, "info test message10")
    append_flash_message(:notification, "info test message12")

    append_flash_message(:error, "error test message20")
    append_flash_message(:error, "error test message22")
     
    haml :"/debug/test", layout: true
  end

  get '/debug/show-sessions-variables' do
    data = ""
    session.each { |k, v| data += "[#{k} --> #{v}]\n"}
    content_type 'text/plain' 
    data
  end

end