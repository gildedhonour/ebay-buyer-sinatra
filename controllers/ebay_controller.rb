require 'sinatra'
require 'sinatra/flash'
require 'json'

class App
  ['/ebay-auth/success', '/ebay-auth/failure', '/ebay/add-new-ebay-account-url'].each do |path|
    before(path) { authenticate! } 
  end

  before '/ebay/*' do
    pass if request.path_info == '/ebay/add-new-ebay-account-url'
    authenticate_and_redirect_to_ebay_login!
  end

  get '/ebay/won' do
    # binding.pry
    if ajax_request?
      render_default_json_response
    else
      @payment_status_list = WonEbayItem.payment_statuses()
      @shipment_status_list = WonEbayItem.shipment_statuses()
      haml(:"/ebay/won-list", layout: true)
    end
  end



  post '/ebay/won' do
    # won list for each of them without filtering
    won_items_all       = EbayAccount.list_items_by_accounts(:won, current_user.selected_ebay_accounts, self)
    new_page_index      = (params[:current_page_index].next.to_i if params[:button_name] == "next_page") || EbayItem::FIRST_PAGE_INDEX

    #categories, conditions and listing types for that won list
    # TODO - REFACTORING!
    cat_cond_list_types               = cat_cond_list_types_by_list(won_items_all)
    category_ids, category_site_ids   = checked_category_ids(cat_cond_list_types[:categories])
    condition_ids, condition_site_ids = checked_condition_ids(cat_cond_list_types[:conditions])
    listing_type_ids                  = cat_cond_list_types[:listing_types]
    return render_json() if any_filter_type_unchecked?(category_site_ids, condition_site_ids, listing_type_ids)

    #filtering
    # TODO - REFACTORING!
    won_items_all_ids = won_items_all.map(&:id)
    items             = EbayItem.where(:won_ebay_item_id.in => won_items_all_ids)
    items             = items.where(:primary_ebay_category.in => category_ids) if category_ids.any? #filtering by category
    items             = items.where(:ebay_condition.in => condition_ids) if condition_ids.any? #filtering by condition
    items             = items.where(:listing_type.in => listing_type_ids) if listing_type_ids.any? #filtering by listing type
    items             = filter_by_price(items) # filter by price
    items             = filter_by_date_range(items) # filter by date range
    items             = items_by_q(items)

    #the final filtered won list 
    # TODO - REFACTORING! 
    won_items_filtered = WonEbayItem.where(:id.in => won_items_all_ids)
                                    .where(:ebay_item_id.in => items.map(&:id))
                                    .where(:ebay_account_id.in => current_user.selected_ebay_accounts.only(&:id).map(&:id))
                                    .desc(:created_date)
                                    .limit(new_page_index * EbayItem::PAGE_SIZE + 1)    #TODO!!! return only specified page
    # binding.pry   # !!!! DEBUG !!!!
                                   
    render_json(item_list: won_items_filtered)
  end

  get '/ebay/watch' do
     
  end
  
  post '/ebay/watch' do
  end


  get '/ebay/best-offers' do
     
  end
  
  post '/ebay/best-offers' do
    
  end

  get '/ebay/lost' do
     
  end

  post '/ebay/lost' do
  end

  get '/ebay/bid' do
 
  end

  post '/ebay/bid' do
  end

  get '/ebay-auth/success' do
    token_data = Ebay::TradingApi.new.request_auth_token_id(session[:ebay_sesssion_id])
    session[:ebay_sesssion_id] = nil
    if token_data[:is_success]
      message = "You have been blessed with unfathomable power!"
      upsert_ebay_account(token_data)
    else
      message = token_data[:value]
    end

    haml :"ebay/auth-success", locals: {messages: message} 
  end

  get '/ebay-auth/failure' do
   session[:ebay_sesssion_id] = nil
   haml :"ebay/auth-failure" 
  end

  # used for ajax requests
  # returns the url of adding new ebay account
  get '/ebay/add-new-ebay-account-url' do
    add_new_ebay_account_url()
  end

  post '/ebay/update-selected-ebay-accounts' do
    if params[:ebay_account]
      current_user.selected_ebay_accounts = EbayAccount.where(:id.in => params[:ebay_account])
      current_user.save!
    end

    render_default_json_response
  end

  private

  #if ebay_account exists then update its token and expire date
  # and add it to current user's ebay_accounts collection if they don't have it yet
  #if it does not exist, create it and add it to current user's ebay_accounts collection
  # TODO - refactor! Move to EbayAccount!!!!!
  def upsert_ebay_account(token_data)
    ebay_account = EbayAccount.where(user_name: params[:username]).first
    if ebay_account
      ebay_account.update_attributes(
                                      token: token_data[:value][:token], 
                                      token_expiration: token_data[:value][:expiration_date]
                                    )
      unless current_user.ebay_accounts.where(user_name: params[:username]).first 
        current_user.ebay_accounts << ebay_account 
      end 
    else
      current_user.ebay_accounts.create!(
                                        user_name: params[:username], 
                                        token: token_data[:value][:token], 
                                        token_expiration: token_data[:value][:expiration_date]
                                        ) 
    end
  end

  def authenticate_and_redirect_to_ebay_login!
    authenticate!
    redirect(add_new_ebay_account_url()) unless current_user.has_ebay_accounts?
  end

  def short_name(nameable_item)
    name = nameable_item.send(:name)
    min_name_length = 20
    name.length > min_name_length ? name[0..min_name_length] + "..." : name
  end

  def checked_category_ids(categories)
    if params[:category]
      [
        EbayCategory.where(:site_id.in => params[:category]).only(:id).map(&:id), 
        categories.select{ |x| params[:category].include?(x[:site_id].to_s()) }
                  .map{ |x| x[:site_id] }
      ]
    else
      [[],[]]
    end   
  end

  # select id and site_id for each condition based on params[:condition]
  # and return them as [[a], [b]]
  def checked_condition_ids(conditions)
    if params[:condition]
      [
        EbayCondition.where(:site_id.in => params[:condition]).only(:id).map(&:id), 
        conditions.select { |x| params[:condition].include?(x[:site_id].to_s()) }
                  .map{ |x| x[:site_id] }
      ]
    else
      [[],[]]
    end
  end

  #TODO - refactoring! Move to module and extend the ...Item
  def items_by_q(items)
    unless params[:q].nil_or_empty?
      q = params[:q].downcase().strip()
      won_items_ids = WonEbayItem.where(seller_email: q).only(:id).map(&:id)
      items.any_of(
                      {:won_ebay_item.in => won_items_ids},
                      {item_id: q},
                      {title: /#{q}/i},
                      {subtitle: /#{q}/i},
                      {seller_user_id: q},
                      {postal_code: q},
                      {location: /#{q}/i}
                    )
    else
      items
    end
  end

  #TODO - refactoring!!! move to ...
  def cat_cond_list_types_by_list(list_items)
    categories, conditions,  listing_types = [], [], []
    ebay_api = Ebay::ShoppingApi.new
    list_items.each do |list_item|
      item = list_item.ebay_item
      unless item.primary_ebay_category or item.ebay_condition
        item_hash = ebay_api.request_single_item(item.item_id)
        
        # if item_hash[:is_success] == false or 
        # list_item.ebay_item is "free" then
        # go to the next step
        next if !item_hash[:is_success] or list_item.ebay_item.shipping_service_cost == 0
        item = item_hash[:value]
      end
#binding.pry   # !!!! DEBUG !!!!
      
      category = {  name:       short_name(item.primary_ebay_category), 
                    full_name:  item.primary_ebay_category.name, 
                    site_id:    item.primary_ebay_category.site_id }
      categories << category unless categories.include?(category)
      
      condition = { name:       short_name(item.ebay_condition), 
                    full_name:  item.ebay_condition.name, 
                    site_id:    item.ebay_condition.site_id }
      conditions << condition unless conditions.include?(condition)
      
      listing_types << item.listing_type unless listing_types.include?(item.listing_type)
    end

    {categories: categories, conditions: conditions, listing_types: listing_types}
  end

  def filter_by_date_range(won_items)
    dates = parse_date_range_parameter()
    return won_items unless dates
    won_items
      .where(:created_date.gte => dates[:start])
      .where(:created_date.lte => dates[:end])
  end

  def parse_date_range_parameter
    # the date format is following:
    # month/day/year - month/day/year
    # "01/09/2013 - 02/07/2013"

    raw_dates = params[:date_range]
    return nil if !raw_dates or raw_dates.empty?
    raw_dates_regex = raw_dates.match(%r{(\d{2}/\d{2}/\d{4})+ - (\d{2}/\d{2}/\d{4})+}i)
    unless raw_dates_regex
      append_flash_message(:error, "The date was not specified correctly. It was ignored.")
      return nil
    end

    start_date, end_date = raw_dates_regex.captures()
    {start: start_date, end: end_date}
  end

  def filter_by_price(items)
    if params[:min_price] || params[:max_price]
      begin
        min_price = Integer(params[:min_price], 10) if params[:min_price]
      rescue ArgumentError
      end

      begin
        max_price = Integer(params[:max_price], 10) if params[:max_price]
      rescue ArgumentError
      end
      
      items = items.where(:current_price.gte => min_price) if min_price
      items = items.where(:current_price.lte => max_price) if max_price
    end

    items
  end
  
  def render_list_view(list_type, options = {})
    EbayAccount::validate_list_type!(list_type)
    
    @list_items               = options[:list_items][0...-1]        || []
    @ebay_accounts            = current_user.ebay_accounts() 
    @categories               = options[:categories]                || []
    @conditions               = options[:conditions]                || []
    @listing_types            = options[:listing_types]             || []
    @checked_category_ids     = options[:checked_category_ids]      || []
    @checked_condition_ids    = options[:checked_condition_ids]     || []
    @checked_listing_type_ids = options[:checked_listing_type_ids]  || []
    @search_q                 = options[:search_q]                  || ""
    @current_page_index       = options[:current_page_index]        || EbayItem::FIRST_PAGE_INDEX
    @next_page_exists         = options[:list_items].to_a.size == @current_page_index * EbayItem::PAGE_SIZE + 1
    @payment_types            = options[:payment_types]             || WonEbayItem.payment_statuses()
    @shipment_types           = options[:shipment_types]            || WonEbayItem.shipment_statuses()
    @checked_payment_type_ids = options[:checked_payment_type_ids]      || []
    @checked_shipment_type_ids = options[:checked_shipment_type_ids]      || []
    @min_price                = options[:min_price]      || ""
    @max_price                = options[:max_price]      || ""
    haml(:"ebay/#{list_type.to_s()}-list", layout: show_layout?)
  end

  def add_new_ebay_account_url
    session_id = Ebay::TradingApi.new.request_session_id[:value]
    session[:ebay_sesssion_id] = session_id
    Ebay::TradingApi.generate_login_url(session_id)
  end

  # check if any filter type(categories, conditions, etc.) is
  # completely unchecked
  def any_filter_type_unchecked?(*filters)
    filters.each { |item| return true if !(item and item.any?) }
    false
  end

  def render_json(options = {})
    item_list = options[:item_list][0...-1] || []
    current_page_index = options[:current_page_index] || EbayItem::FIRST_PAGE_INDEX
    content_type :json
    # binding.pry
    {
      item_list:                              item_list,
      item_list_created_date_pretty:          item_list.map { |item| item.created_date.pretty() },
      item_list_payment_status:               item_list.map { |item| item.payment_status() },
      ebay_item_list:                         item_list.map {|x| x.ebay_item} || [],
      ebay_item_list_shipping_service_display_cost: item_list.map {|x| x.ebay_item.shipping_service_display_cost},
      category_list:      options[:category_list] || [],
      condition_list:     options[:condition_list] || [],
      listing_type_list:  options[:listing_type_list] || [],
      current_page_index: current_page_index,
      next_page_exists:   options[:item_list].to_a.size == current_page_index * EbayItem::PAGE_SIZE + 1
    }.to_json
  end

  def render_default_json_response
    won_items = EbayAccount.list_items_by_accounts(:won, current_user.selected_ebay_accounts, self, false)
    filter_list = cat_cond_list_types_by_list(won_items)
    options = {}
    options[:item_list] = won_items[0...EbayItem::PAGE_SIZE + 1]
    options[:category_list] = filter_list[:categories]
    options[:condition_list] = filter_list[:conditions]
    options[:listing_type_list] = filter_list[:listing_types]
    render_json(options)
  end
end