# This is all the standard CRUD stuff for Users, with a light garnish of
# forgotten password logic.

class App

  helpers do  
    
    # Render correct haml depending on request type
    def render_index(ebay_won)
      @ebay_won = ebay_won
      @ebay_accounts = EbayAccount.where({:user_id => current_user.id})
      haml :"ebay/won-list", :locals => {:ebay_won => @ebay_won, :ebay_accounts => @ebay_accounts}, :layout => show_layout?
    end
  end

end
