class App

  get '/login' do
    redirect '/' if logged_in?
    @return_url = parse_return_url()
    haml :"login/login"
  end

  post '/login' do
    user = User.where(email: params[:email]).first
    if user
      if BCrypt::Password.new(user.hashed_password) == params[:password]
        session[:user_id] = user.id
        # session[:remember_me] = params[:remember_me] #TODO!!!
        unless user.email_confirmed
          append_flash_message(:success, "Welcome back! We've sent you an activation email. Click the link to activate your account and login.")
          Notifier.send_signup_notification(user.email, user.perishable_token) unless development?
        end

        append_flash_message(:success, "You successfully logged in.")
        redirect(parse_return_url())
      else
        append_flash_message(:error, "Your password is incorrect")
        haml :"login/login"
      end

    else
      append_flash_message(:error, "That user email does not exist")
      haml :"login/login"
    end
  end

  get '/signup' do
    redirect '/' if logged_in?
    haml :"login/signup"
  end

  post '/signup' do
    email = params[:email]
    unless User.where(email: email).first
      new_user = User.new(email: email, password: params[:password])
      if new_user.valid?
        if User.valid_password?(params[:password])
          new_user.save!
          session[:user_id] = new_user.id
          Notifier.send_signup_notification(email, new_user.perishable_token) unless development?
          append_flash_message(:success, "Welcome to app.com! We've sent you an activation email to #{email}. Click the link to activate your account and login.")
          redirect "/"
        else
          append_flash_message(:error, "Password is not valid.")
          haml :"login/signup"
        end
      else
        append_flash_message(:error, new_user.errors.full_messages.join("\n"))
        haml :"login/signup"
      end
    else
      append_flash_message(:error, "We already have an account with that email. Please Login or try again.")
      haml :"login/signup"
    end
  end

  get '/logout' do
    if logged_in?
      session[:ebay_sesssion_id] = session[:user_id] = nil
      append_flash_message(:success, "You've been logged out.")
    end

    redirect '/'
  end

  private

  def parse_return_url
    if params[:return_url] && !params[:return_url].nil_or_empty?
      uri = URI(params[:return_url])
      uri.local? ? uri.to_s() : "/"
    else
      "/"
    end
  end
end
