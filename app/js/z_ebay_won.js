String.prototype.format = function() {
    var args = arguments;
    return this.replace(/{(\d+)}/g, function(match, number) { 
      return typeof args[number] != 'undefined'
        ? args[number]
        : match
      ;
    });
  };

function showNoty(text, type){
  if(typeof(type) === 'undefined'){
    type = 'success';
  }
   noty({
          text: text,
          type: type,
          dismissQueue: false,
          layout: 'top',
          theme: 'defaultTheme',
          timeout: true,
          animation: {
                        open: {height: 'toggle'},
                        close: {height: 'toggle'},
                        easing: 'swing',
                        speed: 1000 
                      }
      });
}

function renderTable(data, selector){
    var rows = '';
    for(var i = 0; i < data.ebay_item_list.length; i++){
      var paidForLabelClass = 'label-inverse';
      switch(data.item_list_payment_status[i]){
        case "Paid":
          paidForLabelClass = "label-success";
          break;
        case "Pending":
          paidForLabelClass = "label-warning";
          break;
        case "Unpaid":
          paidForLabelClass = "label-important";
          break;
        case "Refunded":
          paidForLabelClass = "label-info";
          break;
        default:
          paidForLabelClass = "label-inverse";
          break;
      }
       
      var itemIdLabelClass = data.item_list[i].deleted_from_won_list ? "label-important" : "";
      var row = 
      "<tr> \
        <td> \
          <input type='checkbox' /> \
          <a href='#'> \
            <i class='icon-star-empty'>&nbsp;</i> \
          </a> \
         </td> \
         <td> \
          <img src='{0}' /> \
         </td> \
         <td> \
          <p> \
            <a href='{1}'> \
              {2} \
            </a> \
            <br /> \
            <a class='muted' href = 'http://myworld.ebay.com/{3}' \
              <div class = 'small'> \
                {3} ({4}) {5}% \
              </div> \
            </a> \
          </p> \
         </td> \
         <td> \
            <span class = 'label {6}'> \
              ${7} \
            </span> \
            <br /> \
            <div class = 'small muted'> \
              {8} \
            </div> \
         </td> \
         <td> \
          {9} \
         </td> \
         <td> \
          <span class='label {10}'> \
            {11} \
          </span> \
         </td> \
      </tr>"
        .format(
                data.ebay_item_list[i].gallery_url, 
                data.ebay_item_list[i].view_item_url,
                data.ebay_item_list[i].title,
                data.ebay_item_list[i].seller_user_id,
                data.ebay_item_list[i].seller_feedback_score,
                
                data.ebay_item_list[i].seller_feedback_percent,
                paidForLabelClass,
                data.ebay_item_list[i].current_price,
                data.ebay_item_list_shipping_service_display_cost[i],
                data.item_list_created_date_pretty[i],
                
                itemIdLabelClass,
                data.ebay_item_list[i]._id
          );
       
      rows += row;
    }
     
    $(selector).html(rows);
}

function sendInitialAjaxRequest(){
    $("#ajaxModal").modal({keyboard: false, show: true, backdrop: 'static'});
    $.ajax({
        type: "GET",
        url: "/ebay/won",
        success: function(data){
          $("#ajaxModal").modal('hide');
          // debugger;
          if (data.item_list[0] !== undefined){
            $("#data-table-container").show();
            $("#data-table-no-data-container").hide();
            renderTable(data, "#data-table-tbody");
            renderDropDownFilters(data);
            $("#next_page").prop("disabled", !data.next_page_exists);
          } else{
              renderDropDownFilters(undefined);
              $("#data-table-container").hide();
              $("#data-table-no-data-container").show();
          }
          
          showNoty('The data has been retrieved.');
        },
        error: function(data){
          $("#ajaxModal").modal('hide');
          showNoty('The error occured while retrieveing the data!', 'error');
          $("#data-table-container").hide();
          $("#data-table-no-data-container").show();
        }
    });
}

function sendUpdatingEbayAccountsAjaxRequest(){
  var formSerializedData = $("#selected_ebay_accounts_form").serialize();
  $("#ajaxModal").modal('show');
  $.ajax({
      type: "POST",
      url: "/ebay/update-selected-ebay-accounts",
      data: formSerializedData,
      success: function(data){
        $("#ajaxModal").modal('hide');
        if (data.item_list[0] !== undefined){
          $("#data-table-container").show();
          $("#data-table-no-data-container").hide();
          renderTable(data, "#data-table-tbody");
          renderDropDownFilters(data);
          $("#next_page").prop("disabled", !data.next_page_exists);
        } else{
            renderDropDownFilters(undefined);
            $("#data-table-container").hide();
            $("#data-table-no-data-container").show();
        }
        showNoty('The selected ebay accounts have been changed.');
      },
      error: function(data){
        //renderDropDownFilters(undefined);
        $("#ajaxModal").modal('hide');
        showNoty('The error occured!', 'error');
      }
  });
  
  return false;
}

function sendSearchAjaxRequest(button_name){
    var data = $("#search-form").serialize();
    data += "&button_name=" + button_name;
    $.ajax({
        type: "POST",
        url: "/ebay/won",
        data: data,
        success: function(data){
          $("#won-list-container").html(data);
          showNoty('The data was filtered');
        },
        error: function(data){
          showNoty('The error occured, the data was not filtered', 'error');
        }
    });
    
    return false;
  }

function renderDropDownFilters(data){
  renderDropDownFiltersItem(data, 'category');
  renderDropDownFiltersItem(data, 'condition');
  renderDropDownFiltersItem(data, 'listing_type');
}

function renderDropDownFiltersItem(data, filterType){
  var listItemList = '';
  var jsonFilterTypeName = filterType + '_list';
  // debugger;
  if(data !== undefined && data[jsonFilterTypeName][0] !== undefined){
    for(var i = 0; i < data[jsonFilterTypeName].length; i++){
     // debugger;
      var siteId    = data[jsonFilterTypeName][i].site_id || data[jsonFilterTypeName][i];
      var fullName  = data[jsonFilterTypeName][i].full_name || data[jsonFilterTypeName][i];
      var name      = data[jsonFilterTypeName][i].name || data[jsonFilterTypeName][i];
      var listItem = "<li> \
                        <input type='checkbox' value='{0}' title='{1}' name='{3}[]' /> \
                        <label class='checkbox' title='{1}'>{2}</label> \
                      </li>"
                    .format(siteId, fullName, name, filterType);
      listItemList += listItem;
    }

    listItemList +=  "<li class='divider'></li>" + 
                      "<li>" + 
                        "<input type='checkbox' id='dropdown-menu-" + filterType +"-chk' />" +
                        "<label class='checkbox' title='Check / Uncheck all'>Check / Uncheck all</label>" +
                      "</li>";
    } 
    else{
      listItemList = "<li><label>No items</label>";
    }

  $("#dropdown-menu-" + filterType).html(listItemList);
}

$(document).ready(function() {
  sendInitialAjaxRequest();

  $(document).on("click", "#apply_filters", function(){
    sendSearchAjaxRequest("apply_filters");
    return true;
  });
  
  $(document).on("change", "#table-check-all-chk", function(){
      $(".table.table-hover.table-condensed input[type=checkbox]").prop('checked', this.checked);
    });

  $(document).on("change", "#dropdown-menu-category-chk", function(){
       $("#dropdown-menu-category input[type=checkbox]").prop('checked', this.checked);
    });

  $(document).on("change", "#dropdown-menu-condition-chk", function(){
       $("#dropdown-menu-condition input[type=checkbox]").prop('checked', this.checked);
    });

  $(document).on("change", "#dropdown-menu-listing_type-chk", function(){
       $("#dropdown-menu-listing_type input[type=checkbox]").prop('checked', this.checked);
    });

  $(document).on("focus", "#date-range-picker", function(e){
    $(e.currentTarget).daterangepicker(
        {
          ranges: {
              'Today': ['today', 'today'],
              'Yesterday': ['yesterday', 'yesterday'],
              'Last 7 Days': [Date.today().add({ days: -6 }), 'today'],
              'Last 30 Days': [Date.today().add({ days: -29 }), 'today'],
              'This Month': [Date.today().moveToFirstDayOfMonth(), Date.today().moveToLastDayOfMonth()],
              'Last Month': [Date.today().moveToFirstDayOfMonth().add({ months: -1 }), Date.today().moveToFirstDayOfMonth().add({ days: -1 })]
          }
        },
        function(start, end) {
            $('#reportrange span').html(start.toString('MMMM d, yyyy') + ' - ' + end.toString('MMMM d, yyyy'));
        } 
      );
   });

  $(document).on("click", "#next_page", function(){
     sendSearchAjaxRequest("next_page");
     return true;
  });

  $("#btn_selected_ebay_accounts").click(function(){
     sendUpdatingEbayAccountsAjaxRequest();
     return true;
  });

  $("#remove_filters").click(function(){
     sendInitialAjaxRequest();
     return true;
  });

  $("#add_ebay_account").click(function(){
      $.get('/ebay/add-new-ebay-account-url', function(data){ 
       window.location = data;
    });
  });

});