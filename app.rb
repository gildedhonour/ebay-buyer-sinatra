require_relative "app_base"

class App
  get "/" do
    if logged_in?
      return haml :"static/welcome" unless current_user.email_confirmed
      current_user.has_ebay_accounts? ? redirect('/ebay/won') : haml(:"static/connect")
    else
      haml :"static/home"
    end
  end

  not_found do
    haml :"static/error", locals: {code: 404, message: "We couldn't find the page you are looking for"}
  end

  error do
    haml :"static/error", locals: {code: 500, message: "Something went wrong"}
  end
end